package run.lambda.periodically;

import com.amazonaws.greengrass.dao.DefaultRunProcedurePeriodicallyDao;
import com.amazonaws.greengrass.dao.RunProcedurePeriodically;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class LambdaFunctionHandler implements RequestHandler<Void, String> {

	private final RunProcedurePeriodically runProcedure = DefaultRunProcedurePeriodicallyDao.instance;

	@Override
	public String handleRequest(Void nothing, Context context) {

		LambdaLogger log = context.getLogger();

		log.log("Before running procedure");

		boolean isRan = runProcedure.runProcedure("call calculate_downtime2(1,2,0)");

		if (isRan) {
			log.log("Procedure running successfully");
		} else {
			log.log("Procedure running failed please check logs");
		}

		return "Procedure Executed successfully";
	}
}

package com.amazonaws.greengrass.conf;

import java.io.InputStream;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

/**
 * Holds a configuration loaded from the {@code application.yml} file on the
 * classpath.
 * 
 * Reason behind to make enum here to make class as singleton
 * 
 * @author Dinesh
 */


public enum ConfigurationHolder {
	instance;
	private final Yaml yaml = new Yaml(new Constructor(Configuration.class));
	private Configuration configuration;

	ConfigurationHolder() {
		load();
	}

	private void load() {
		InputStream inputStream = ConfigurationHolder.class.getResourceAsStream("/resources.yaml");

		this.configuration = (Configuration) yaml.load(inputStream);
	}

	public Configuration configuration() {
		return this.configuration;
	}
}

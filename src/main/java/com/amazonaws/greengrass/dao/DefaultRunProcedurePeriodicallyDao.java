package com.amazonaws.greengrass.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.amazonaws.greengrass.db.Database;

/**
 * DefaultEmiMachineDao by implementing Emi Machine Dao
 * 
 * @author Dinesh
 * @since Jun 27, 2019
 *
 */
public enum DefaultRunProcedurePeriodicallyDao implements RunProcedurePeriodically {
	instance;

	@Override
	public boolean runProcedure(String name) {
		try (Connection conn = Database.connection(); 
				CallableStatement stmt = conn.prepareCall("{" + name + "}")) {
			return stmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}

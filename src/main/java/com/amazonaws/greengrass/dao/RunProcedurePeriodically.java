package com.amazonaws.greengrass.dao;

public interface RunProcedurePeriodically {

	boolean runProcedure(String name);
}

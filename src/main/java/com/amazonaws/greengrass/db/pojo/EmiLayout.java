package com.amazonaws.greengrass.db.pojo;

public class EmiLayout {
	private Integer pkLayout;
	private String layoutName;
	private String layoutDescription;
	private Integer fkSiteId;

	/*-	
	 * Eclipse will not auto format if we enclose java code in <pre>/*-</pre>
	 * Can be un-commented if it required for to select all records
	 * 
	public EmiLayout(Integer pkLayout, String layoutName, String layoutDescription, Integer fkSiteId) {
		super();
		this.pkLayout = pkLayout;
		this.layoutName = layoutName;
		this.layoutDescription = layoutDescription;
		this.fkSiteId = fkSiteId;
	}
	
	
	public static EmiLayout of(int pkLayout, String layoutName, String layoutDescription, int fkSiteId) {
		return new EmiLayout(pkLayout, layoutName, layoutDescription, fkSiteId);
	}
	*/

	public Integer getPkLayout() {
		return pkLayout;
	}

	public void setPkLayout(Integer pkLayout) {
		this.pkLayout = pkLayout;
	}

	public String getLayoutName() {
		return layoutName;
	}

	public void setLayoutName(String layoutName) {
		this.layoutName = layoutName;
	}

	public String getLayoutDescription() {
		return layoutDescription;
	}

	public void setLayoutDescription(String layoutDescription) {
		this.layoutDescription = layoutDescription;
	}

	public Integer getFkSiteId() {
		return fkSiteId;
	}

	public void setFkSiteId(Integer fkSiteId) {
		this.fkSiteId = fkSiteId;
	}

}

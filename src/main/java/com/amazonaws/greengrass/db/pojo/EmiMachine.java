package com.amazonaws.greengrass.db.pojo;

public class EmiMachine {

	private Integer machineId;
	private String machineName;
	private Integer siteId;
	private String machineOwnerName;
	private String companyName;
	private String modelNumber;
	private String machineSupportPhone;
	private String machineSupportEmail;
	private String machineOwnerPhone;
	
	/*-
	 * Eclipse will not auto format if we enclose java code in <pre>/*-</pre>
	 * Can be un-commented if it required for to select all records
	 * 
	public EmiMachine(Integer machineId, String machineName, Integer siteId, String machineOwnerName,
			String companyName, String modelNumber, String machineSupportPhone, String machineSupportEmail,
			String machineOwnerPhone) {
		super();
		this.machineId = machineId;
		this.machineName = machineName;
		this.siteId = siteId;
		this.machineOwnerName = machineOwnerName;
		this.companyName = companyName;
		this.modelNumber = modelNumber;
		this.machineSupportPhone = machineSupportPhone;
		this.machineSupportEmail = machineSupportEmail;
		this.machineOwnerPhone = machineOwnerPhone;
	}
	
	public static EmiMachine of(Integer machineId, String machineName, Integer siteId, String machineOwnerName,
			String companyName, String modelNumber, String machineSupportPhone, String machineSupportEmail,
			String machineOwnerPhone) {
		return new EmiMachine(machineId, machineName, siteId, machineOwnerName, companyName, modelNumber,
				machineSupportPhone, machineSupportEmail, machineOwnerPhone);
	
	}
	*/

	public Integer getMachineId() {
		return machineId;
	}

	public void setMachineId(Integer machineId) {
		this.machineId = machineId;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public String getMachineOwnerName() {
		return machineOwnerName;
	}

	public void setMachineOwnerName(String machineOwnerName) {
		this.machineOwnerName = machineOwnerName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getMachineSupportPhone() {
		return machineSupportPhone;
	}

	public void setMachineSupportPhone(String machineSupportPhone) {
		this.machineSupportPhone = machineSupportPhone;
	}

	public String getMachineSupportEmail() {
		return machineSupportEmail;
	}

	public void setMachineSupportEmail(String machineSupportEmail) {
		this.machineSupportEmail = machineSupportEmail;
	}

	public String getMachineOwnerPhone() {
		return machineOwnerPhone;
	}

	public void setMachineOwnerPhone(String machineOwnerPhone) {
		this.machineOwnerPhone = machineOwnerPhone;
	}

}

package com.amazonaws.greengrass.db.pojo;

import java.util.Date;

public class EmiDowntimeFact {

	private Integer pkDowntime;
	private Date stopTime;
	private Date startTime;
	private Integer fkLayout;
	private Integer fkMachine;
	private Integer fkSite;
	private Integer feederError;
	private Integer manualFiducialSearch;
	private Integer automaticFiducialSearch;
	private Integer localFiducialSearch;
	private Integer noComponent;
	private Integer systemError;
	private String reason;

	/*-
	 * Eclipse will not auto format if we enclose java code in <pre>/*-</pre>
	 * Can be un-commented if it required for to select all records
	 * 
		public EmiDowntimeFact(Integer pkDowntime, Date stopTime, Date startTime, Integer fkLayout, Integer fkMachine,
				Integer fkSite, Integer feederError, Integer manualFiducialSearch, Integer automaticFiducialSearch,
				Integer localFiducialSearch, Integer noComponent, Integer systemError) {
			super();
			this.pkDowntime = pkDowntime;
			this.stopTime = stopTime;
			this.startTime = startTime;
			this.fkLayout = fkLayout;
			this.fkMachine = fkMachine;
			this.fkSite = fkSite;
			this.feederError = feederError;
			this.manualFiducialSearch = manualFiducialSearch;
			this.automaticFiducialSearch = automaticFiducialSearch;
			this.localFiducialSearch = localFiducialSearch;
			this.noComponent = noComponent;
			this.systemError = systemError;
		}
		
		public EmiDowntimeFact of(Integer pkDowntime, Date stopTime, Date startTime, Integer fkLayout, Integer fkMachine,
				Integer fkSite, Integer feederError, Integer manualFiducialSearch, Integer automaticFiducialSearch,
				Integer localFiducialSearch, Integer noComponent, Integer systemError) {
			return new EmiDowntimeFact(pkDowntime, stopTime, startTime, fkLayout, fkMachine, fkSite, feederError,
					manualFiducialSearch, automaticFiducialSearch, localFiducialSearch, noComponent, systemError);
		}
		
		*/

	public Integer getPkDowntime() {
		return pkDowntime;
	}

	public void setPkDowntime(Integer pkDowntime) {
		this.pkDowntime = pkDowntime;
	}

	public Date getStopTime() {
		return stopTime;
	}

	public void setStopTime(Date stopTime) {
		this.stopTime = stopTime;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Integer getFkLayout() {
		return fkLayout;
	}

	public void setFkLayout(Integer fkLayout) {
		this.fkLayout = fkLayout;
	}

	public Integer getFkMachine() {
		return fkMachine;
	}

	public void setFkMachine(Integer fkMachine) {
		this.fkMachine = fkMachine;
	}

	public Integer getFkSite() {
		return fkSite;
	}

	public void setFkSite(Integer fkSite) {
		this.fkSite = fkSite;
	}

	public Integer getFeederError() {
		return feederError;
	}

	public void setFeederError(Integer feederError) {
		this.feederError = feederError;
	}

	public Integer getManualFiducialSearch() {
		return manualFiducialSearch;
	}

	public void setManualFiducialSearch(Integer manualFiducialSearch) {
		this.manualFiducialSearch = manualFiducialSearch;
	}

	public Integer getAutomaticFiducialSearch() {
		return automaticFiducialSearch;
	}

	public void setAutomaticFiducialSearch(Integer automaticFiducialSearch) {
		this.automaticFiducialSearch = automaticFiducialSearch;
	}

	public Integer getLocalFiducialSearch() {
		return localFiducialSearch;
	}

	public void setLocalFiducialSearch(Integer localFiducialSearch) {
		this.localFiducialSearch = localFiducialSearch;
	}

	public Integer getNoComponent() {
		return noComponent;
	}

	public void setNoComponent(Integer noComponent) {
		this.noComponent = noComponent;
	}

	public Integer getSystemError() {
		return systemError;
	}

	public void setSystemError(Integer systemError) {
		this.systemError = systemError;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}

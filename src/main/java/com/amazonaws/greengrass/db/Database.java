package com.amazonaws.greengrass.db;

import java.sql.Connection;
import java.sql.SQLException;

import com.amazonaws.greengrass.conf.Configuration;
import com.amazonaws.greengrass.conf.ConfigurationHolder;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public final class Database {

	private Database() {
		throw new AssertionError();
	}

	private static final HikariDataSource dataSource;
	private static int poolSize = 40;


	static {
		Configuration configuration = ConfigurationHolder.instance.configuration();
		Configuration.DataSource props = configuration.getDatasource();

		HikariConfig config = new HikariConfig();
		config.setDriverClassName(props.getDriverClassName());
		config.setJdbcUrl(props.getUrl());
		config.setUsername(props.getUsername());
		config.setPassword(props.getPassword());
		config.setMaximumPoolSize(poolSize);


		

		dataSource = new HikariDataSource(config);
	}

	public static Connection connection() throws SQLException {
		return dataSource.getConnection();
	}
}

package event.start.delta.lambda.pojo;

import java.util.List;

public class EventStartStop {

	private String layout;
	private String trpTime;
	private List<String> reasons;
	private String machine;
	private String startTimestamp;
	private String stopTimestamp;
	private String reason;

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getTrpTime() {
		return trpTime;
	}

	public void setTrpTime(String trpTime) {
		this.trpTime = trpTime;
	}

	public List<String> getReasons() {
		return reasons;
	}

	public void setReasons(List<String> reasons) {
		this.reasons = reasons;
	}

	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}

	public String getStartTimestamp() {
		return startTimestamp;
	}

	public void setStartTimestamp(String startTimestamp) {
		this.startTimestamp = startTimestamp;
	}

	public String getStopTimestamp() {
		return stopTimestamp;
	}

	public void setStopTimestamp(String stopTimestamp) {
		this.stopTimestamp = stopTimestamp;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
